#!/bin/bash

#Powered by Bastien T

#Sentence : BONJOUR
#Size : 50

#Character B
xeyes -geometry 50x50-1000+300 & 
xeyes -geometry 50x50-950+300 & 
xeyes -geometry 50x50-900+300 & 
xeyes -geometry 50x50-1000+350 & 
xeyes -geometry 50x50-850+350 & 
xeyes -geometry 50x50-1000+400 & 
xeyes -geometry 50x50-950+400 & 
xeyes -geometry 50x50-900+400 & 
xeyes -geometry 50x50-1000+450 & 
xeyes -geometry 50x50-850+450 & 
xeyes -geometry 50x50-1000+500 & 
xeyes -geometry 50x50-950+500 & 
xeyes -geometry 50x50-900+500 & 
#Character O
xeyes -geometry 50x50-700+300 & 
xeyes -geometry 50x50-650+300 & 
xeyes -geometry 50x50-750+350 & 
xeyes -geometry 50x50-600+350 & 
xeyes -geometry 50x50-750+400 & 
xeyes -geometry 50x50-600+400 & 
xeyes -geometry 50x50-750+450 & 
xeyes -geometry 50x50-600+450 & 
xeyes -geometry 50x50-700+500 & 
xeyes -geometry 50x50-650+500 & 
#Character N
xeyes -geometry 50x50-500+300 & 
xeyes -geometry 50x50-350+300 & 
xeyes -geometry 50x50-500+350 & 
xeyes -geometry 50x50-450+350 & 
xeyes -geometry 50x50-350+350 & 
xeyes -geometry 50x50-500+400 & 
xeyes -geometry 50x50-400+400 & 
xeyes -geometry 50x50-350+400 & 
xeyes -geometry 50x50-500+450 & 
xeyes -geometry 50x50-350+450 & 
xeyes -geometry 50x50-500+500 & 
xeyes -geometry 50x50-350+500 & 
#Character J
xeyes -geometry 50x50-100+300 & 
xeyes -geometry 50x50-100+350 & 
xeyes -geometry 50x50-100+400 & 
xeyes -geometry 50x50-250+450 & 
xeyes -geometry 50x50-100+450 & 
xeyes -geometry 50x50-200+500 & 
xeyes -geometry 50x50-150+500 & 
#Character O
xeyes -geometry 50x50--50+300 & 
xeyes -geometry 50x50--100+300 & 
xeyes -geometry 50x50-0+350 & 
xeyes -geometry 50x50--150+350 & 
xeyes -geometry 50x50-0+400 & 
xeyes -geometry 50x50--150+400 & 
xeyes -geometry 50x50-0+450 & 
xeyes -geometry 50x50--150+450 & 
xeyes -geometry 50x50--50+500 & 
xeyes -geometry 50x50--100+500 & 
#Character U
xeyes -geometry 50x50--250+300 & 
xeyes -geometry 50x50--400+300 & 
xeyes -geometry 50x50--250+350 & 
xeyes -geometry 50x50--400+350 & 
xeyes -geometry 50x50--250+400 & 
xeyes -geometry 50x50--400+400 & 
xeyes -geometry 50x50--250+450 & 
xeyes -geometry 50x50--400+450 & 
xeyes -geometry 50x50--250+500 & 
xeyes -geometry 50x50--300+500 & 
xeyes -geometry 50x50--350+500 & 
xeyes -geometry 50x50--400+500 & 
#Character R
xeyes -geometry 50x50--500+300 & 
xeyes -geometry 50x50--550+300 & 
xeyes -geometry 50x50--600+300 & 
xeyes -geometry 50x50--500+350 & 
xeyes -geometry 50x50--650+350 & 
xeyes -geometry 50x50--500+400 & 
xeyes -geometry 50x50--550+400 & 
xeyes -geometry 50x50--600+400 & 
xeyes -geometry 50x50--500+450 & 
xeyes -geometry 50x50--600+450 & 
xeyes -geometry 50x50--500+500 & 
xeyes -geometry 50x50--650+500 & 
