#!/usr/bin/env python

from functions import * #import functions
from resources import * #import resources

import sys #args

#check syntax
if (len(sys.argv) < 2 or len(sys.argv) > 3):
    print("Syntax error : python xeyes_sentences.py \"sentence_to_display\" [size]")
    exit(0)

#sentences to display
s_input = sys.argv[1];
s_input = s_input.upper()
s_input = s_input.replace('""', "")

#if several sentences : delimiter "\"
l_sentences = s_input.split("\\")

initFile(s_input)

for s_sentence in l_sentences:
    displaySentence(s_sentence)
